import { PROFILE_COLLAPSIBLE_UPDATE } from '../constants'

export const updateCollapsibleButton = () => (dispatch) => {
  dispatch({ type: PROFILE_COLLAPSIBLE_UPDATE });
};