import React, { PureComponent } from 'react';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { View, Dimensions, StyleSheet, } from 'react-native'
import { connect } from 'react-redux';
import * as userActions from '../../actions/userActions';
import * as profileActions from '../../actions/profileActions';
import InputRightIconGeneric from '../common/fields/InputRightIcon';

class StartPerfilAlturaForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {value: '', loadedValue: false};
  }

  componentWillMount() {
    if(!this.state.loadedValue && this.props.profile.height){
      this.setState({value: Number(this.props.profile.height).toFixed(2).replace(/\./, ','), loadedValue: true})
    }
  }

  handleFormSubmit = () => {
  };

  normalizeHeight = (value) => {
    if (!value || typeof value !== 'string') {
      return value
    }

    const onlyNums = value.replace(/[^\d]/g, '');
    if (onlyNums.length <= 1) {
      return onlyNums
    } else {
      return `${onlyNums.slice(0, 1)},${onlyNums.slice(1,3)}`
    }
  };

  render() {
    return (
      <View style={startPerfilAlturaFormStyle.mainHolder}>
        <InputRightIconGeneric
          name="height"
          label="Minha altura atual (m)"
          fieldLineColor="#CCCCCC"
          imagePath={require('../../lib/Images/show_chart.png')}
          imageText="ver histórico"
          buttonAction={() => { /* TODO */ }}
          onChange={ (value) => this.setState({ value: this.normalizeHeight(value) }) }
          value={this.state.value}
          onEndEditing={() => {/* todo */ }}
          keyboardType="numeric"
          placeholder="Ex: 1,70"
          editable
        />
      </View>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    dispatch: dispatch,
  };
};

const startPerfilAlturaFormStyle = StyleSheet.create({
  mainHolder: {
    width: Dimensions.get('window').width - 20,
    flexDirection: 'column',
    alignItems: 'center',
    height: 90,
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(StartPerfilAlturaForm);
