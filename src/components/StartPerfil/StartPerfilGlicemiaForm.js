import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { View, Dimensions, StyleSheet, } from 'react-native'
import { connect } from 'react-redux';
import * as userActions from '../../actions/userActions';
import * as profileActions from '../../actions/profileActions';
import InputRightIconGeneric from '../common/fields/InputRightIcon';

class StartPerfilGlicemiaForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '', loadedValue: false};
  }

  render() {
    return (
      <View
        style={startPerfilGlicemiaFormStyle.mainHolder}
      >
        <InputRightIconGeneric
          name="glycemia"
          label="Glicemia capilar (mg/dl)"
          fieldLineColor="#CCCCCC"
          imagePath={require('../../lib/Images/show_chart.png')}
          imageText="ver histórico"
          buttonAction={() => { /* TODO */ }}
          onChange={ (value) => this.setState({ value }) }
          value={this.state.value}
          onEndEditing={() => { /* TODO */ }}
          keyboardType="numeric"
          placeholder="Ex: 90"
          editable
        />
      </View>
    );
  };
}


const mapStateToProps = (state) => {
  return {
    user: state.user,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    dispatch: dispatch,
  };
};

const startPerfilGlicemiaFormStyle = StyleSheet.create({
  mainHolder: {
    width: Dimensions.get('window').width - 20,
    flexDirection: 'column',
    alignItems: 'center',
    height: 90,
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(StartPerfilGlicemiaForm);
