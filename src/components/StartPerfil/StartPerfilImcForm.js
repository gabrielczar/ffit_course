import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { View, Dimensions, StyleSheet, Text, } from 'react-native'
import { connect } from 'react-redux';
import * as userActions from '../../actions/userActions';
import * as profileActions from '../../actions/profileActions';
import InputRightIconGeneric from '../common/fields/InputRightIcon';

class StartPerfilImcForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '', weight: '', height: '' };
  }

  update() {
    let weight = this.props.profile.weight;
    let height = this.props.profile.height;
    let value  = this.calcIMC(weight, height);

    this.setState({ weight, height, value });
  }

  calcIMC(weight, height) {
    return weight / (height * height);
  }

  render() {
    return (
      <View style={startPerfilImcFormStyle.mainHolder}>
        <InputRightIconGeneric
          name="imc"
          label="Meu IMC atual"
          labelColor="#FFFFFF"
          fieldLineColor="#FFFFFF"
          inputTextColor="#FFFFFF"
          placeholderTextColor="#FFFFFF"
          imagePath={require('../../lib/Images/show_chart.png')}
          imageText="ver histórico"
          imageTextColor="#FFFFFF"
          buttonAction={() => { /* TODO */ }}
          onChange={ () => this.update() }
          value={this.state.value}
          keyboardType="numeric"
          placeholder="Ex: 24,22"
          editable={false}
        />
      </View>
    );
  };
}

const startPerfilImcFormStyle = StyleSheet.create({
  mainHolder: {
    width: Dimensions.get('window').width - 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
    backgroundColor: '#FFA000',
  },
});

const mapStateToProps = (state) => {
  return {
    user: state.user,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StartPerfilImcForm);
