import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { View, Dimensions } from 'react-native'
import { connect } from 'react-redux';
import * as userActions from '../../actions/userActions';
import * as profileActions from '../../actions/profileActions';
import InputRightIconGeneric from '../common/fields/InputRightIcon';

class StartPerfilPressaoForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { systolic: '', diastolic: '', loadedValue: false};
  }

  render() {
    const window = Dimensions.get('window');
    return (
      <View style={{flex: 1, flexDirection: 'column', alignItems: 'center',}}>
        <View style={{ height: 90,}}>
          <InputRightIconGeneric
            name="systolic"
            label="Pressão arterial sistólica (mmHg)"
            fieldLineColor="#CCCCCC"
            imagePath={require('../../lib/Images/show_chart.png')}
            imageText="ver histórico"
            buttonAction={() => { /* TODO */ } }
            onChange={ (value) => this.setState({ value }) }
            value={this.state.systolic}
            onEndEditing={() => { /* TODO */ }}
            keyboardType="numeric"
            placeholder="Ex: 120"
            editable
          />
        </View>

        <View>
          <InputRightIconGeneric
            name="diastolic"
            label="Pressão arterial diastólica (mmHg)"
            fieldLineColor="#CCCCCC"
            imagePath={require('../../lib/Images/show_chart.png')}
            imageText="ver histórico"
            buttonAction={() => {/* TODO */}}
            onChange={(value) => this.setState({ value })}
            value={this.state.diastolic}
            onEndEditing={() => { /* TODO */ }}
            keyboardType="numeric"
            placeholder="Ex: 80"
            editable
          />
        </View>
      </View>
    );
  };
}


const mapStateToProps = (state) => {
  return {
    user: state.user,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StartPerfilPressaoForm);
