import React, { PureComponent } from 'react';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Text, Dimensions, View, ScrollView, Platform, TouchableOpacity, KeyboardAvoidingView, Alert, } from 'react-native';
import { Icon, Left, Right, Content } from 'native-base';
import { MenuProvider } from 'react-native-popup-menu';
import Toast from 'react-native-easy-toast';
import { Actions } from 'react-native-router-flux';
import AndroidKeyboardAdjust from 'react-native-android-keyboard-adjust';

import Thumbnail from '../common/view/Thumbnail';
import RightMenu from '../common/menu/RightMenu';
import Header from '../common/header';
import Container from '../common/container';

import StartPerfilPesoForm from './StartPerfilPesoForm';
import StartPerfilAlturaForm from './StartPerfilAlturaForm';
import StartPerfilImcForm from './StartPerfilImcForm';
import StartPerfilGlicemiaForm from './StartPerfilGlicemiaForm';
import StartPerfilPressaoForm from './StartPerfilPressaoForm';
import StartPerfilFrequenciaCardiacaForm from './StartPerfilFrequenciaCardiacaForm';
import StartPerfilTemperaturaForm from './StartPerfilTemperaturaForm';

import * as userActions from '../../actions/userActions';
import * as profileActions from '../../actions/profileActions';
import * as ffitActions from '../../actions/ffitAction';

import Style from '../../utils/responsiveFactor';
import DefaultLoader from '../common/loader/DefaultLoader';
import CustomStatusBar from '../common/statusbar/CustomStatusBar';
import PerfilButton from '../common/button/ButtonWithTwoTextLinesAndBottomIcon';

import Collapsible from 'react-native-collapsible';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { 
  PROFILE_COLLAPSIBLE_IMC,
  PROFILE_COLLAPSIBLE_DEMAIS_INFO,
  PROFILE_COLLAPSIBLE_PRESSAO_ARTERIAL
} from '../../constants'

class StartPerfilScreen extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      active: false
    }
  }

  renderRightMenu() {
    let menus = [
      {
        text: 'Sair',
        action: () => { this.props.userActions.signoutUser(); }
      }
    ];

    return (
      <RightMenu menus={menus} iconName="md-more"
         customStyles={{
           triggerTouchable: {
             hitSlop: {top: 10, bottom: 10, right: 10, left: 10,}
           },
           triggerWrapper: {width: 40, alignItems: 'center', justifyContent: 'center'}
         }}
         menuOptionsStyles={{
           optionsContainer: {width: 180}
         }}
      />
    );
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      AndroidKeyboardAdjust.setAdjustPan();
    }
  }

  componentDidMount() {
    this.props.profileActions.getProfileInfo();
    //this.props.ffitActions.updateFfitToken();
  }


  fetchSections() {
    return [
      {
        title: PROFILE_COLLAPSIBLE_IMC,
        components: [
            <StartPerfilPesoForm key='peso' value={this.props.profile.weight} />,
            <StartPerfilAlturaForm key='altura' value={this.props.profile.height} />,
            <StartPerfilImcForm key='imc'
                weight={this.props.profile.weight} 
                height={this.props.profile.height} />,
        ]
      },
      {
        title: PROFILE_COLLAPSIBLE_PRESSAO_ARTERIAL,
        components: [
          <StartPerfilPressaoForm key='pressao'
                      systolic={this.props.profile.blood_pressure.systolic}
                      diastolic={this.props.profile.blood_pressure.diastolic}
            />,
        ]
      },
      {
        title: PROFILE_COLLAPSIBLE_DEMAIS_INFO,
        components: [
          <StartPerfilFrequenciaCardiacaForm key='cardio' value={this.props.profile.heart_beat} />,
          <StartPerfilGlicemiaForm key='clicemia' value={this.props.profile.glycemia} />,
          <StartPerfilTemperaturaForm key='temperatura' value={this.props.profile.temperature} />,
        ]
      }
    ];

  }

  changeCollapsed() {
    this.props.ffitActions.updateCollapsibleButton();

    this.setState({ active : !this.state.active });
  }

  renderCollapsibles() {
    let sections = this.fetchSections();

    let collapsed = this.props.ffit.collapsed;

    return (
      <View>
        { sections.map(data => 
            <View key={data.title}>

              <TouchableOpacity onPress={() => this.changeCollapsed() }
                style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 1, 
                  borderBottomColor: '#e0e0e0',  }}>
                <Left>  
                  <Text
                    style={{ 
                      textAlign: 'left', 
                      paddingVertical: 20, 
                      paddingLeft: 0,
                      fontSize: Style.FONT_SIZE_20, color: '#5C6BC0', fontWeight: '100',}}>
                    
                    {data.title} 
                  </Text>
                </Left>
                <Right>
                  <FontAwesome style={{fontSize: 20, color: '#5C6BC0',}}>
                    { collapsed
                    ? (Icons.angleDown)
                    : (Icons.angleUp)
                    }
                  </FontAwesome>
                </Right>
              </TouchableOpacity>
              
              <Collapsible collapsed={collapsed} style={{paddingTop: 20, }}>
                  { data.components }
              </Collapsible>

            </View>)}
      </View>
    );
  }

  render() {
    const window = Dimensions.get('window');
    
    return (
      <MenuProvider>
        <Header>
          <CustomStatusBar bgColor={"#5C6BC0"} theme={(Platform.OS === 'ios') ? 'dark-content' : 'light-content'} />
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 0.7, flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center', marginLeft: 20,}}>
              <Text allowFontScaling={false} style={{fontSize: Style.FONT_SIZE_20, color: '#868686'}}>Início</Text>
            </View>

            <View style={{flex: 0.3, flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'center',}}>

              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 0.5, flexDirection: 'column', alignItems: 'center',}}>
                  {this.renderRightMenu()}
                </View>
              </View>
            </View>
          </View>
        </Header>

        <Container>
          <KeyboardAvoidingView style={{flex: 1,}} behavior="padding" keyboardVerticalOffset={(Platform.OS === 'ios') ? 80 : -90}>
            <ScrollView>
              <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1, paddingBottom: 10}}>
                <View style={{marginTop: 10, flexDirection: 'row', width: (window.width-20),}}>
                  <View style={{marginTop: 5,}}>
                    <TouchableOpacity onPress={() => { /* TODO */ }}>
                      <Thumbnail width={48} height={48} backgroundColor="#F1F1F1" imagePath={(this.props.profile.profile_picture) ? { uri: this.props.profile.profile_picture } : require('../../lib/Images/AVATAR_MRES.png')}
                                 imageWidth={48} imageHeight={48} />
                    </TouchableOpacity>
                  </View>

                  <View style={{flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 10,}}>
                    <Text allowFontScaling={false} style={{fontSize: Style.FONT_SIZE_24, color: '#5C6BC0', fontWeight: '100', width: (window.width-100)}}>{this.props.profile.name}</Text>
                    <Text allowFontScaling={false} style={{fontSize: Style.FONT_SIZE_13, color: '#5C6BC0', fontWeight: '100',}}>{this.props.profile.cpf}</Text>
                  </View>
                </View>

                {/* ---------- Main Menu ---------- */}

                <View style={{width: (window.width - 20), flex: 1, flexDirection: 'row', marginTop: 20,}}>
                  <PerfilButton
                    upperText="Minhas"
                    bottomText="Alergias"
                    image={require('../../lib/Images/icon_lupa_active.png')}
                  />

                  <PerfilButton
                    onPress={Actions.medicines_group}
                    upperText="Meus"
                    bottomText="Medicamentos"
                    bottomTextStyle={{fontSize: Style.FONT_SIZE_11, fontWeight: 'bold'}}
                    image={require('../../lib/Images/icon_pilula_active.png')}
                  />

                  <PerfilButton
                    upperText="Doenças"
                    bottomText="Crônicas"
                    image={require('../../lib/Images/icon_chronic_disease_active.png')}
                  />
                </View>
                
                {/* ---------- Collapsible Menus ---------- */}
                
                <View style={{ width: (window.width - 20), flex: 1, flexDirection: 'column',}}>
                  { this.renderCollapsibles() }
                </View>
                
                {/* ---------- End Collapsible Menus ---------- */}
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </Container>

        <DefaultLoader visible={this.props.loader.active.status} label={this.props.loader.active.msg} />
        <Toast ref="toast" opacity={0.6} position='center' />
      </MenuProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    profile: state.profile,
    loader: state.loader,
    ffit: state.ffit
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    ffitActions: bindActionCreators(ffitActions, dispatch),
    dispatchActions: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StartPerfilScreen);
