import { PROFILE_COLLAPSIBLE_UPDATE } from '../constants'

const INITIAL_STATE = {
  collapsed: true
};

export default function ffitReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
      case PROFILE_COLLAPSIBLE_UPDATE:
        return {
          ...state, 
          collapsed: !state.collapsed
        };
      default:
        return state;
  }
}
