import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import loaderReducer from './loaderReducer';
import userReducer from './userReducer';
import profileReducer from './profileReducer';
import medicineReducer from './medicineReducer';
import ffitReducer from './ffitReducer';

const appReducer = combineReducers({
  form: formReducer,

  ffit: ffitReducer,
  loader: loaderReducer,
  user: userReducer,
  profile: profileReducer,
  medicine: medicineReducer,
});

export default appReducer;
